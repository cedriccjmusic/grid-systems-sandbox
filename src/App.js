import logo from "./logo.svg";
import "./App.css";
import ReactGrid1 from "./ReactGrid1";
import { withTheme } from "@sherwin-williams/mosaic-component-library";
import W3Grid from "./W3Grid";
import Flexbox from "./Flexbox";

function App() {
  return (
    <>
      <ReactGrid1 />
      <W3Grid />
      <Flexbox />
    </>
  );
}

export default withTheme(App);
