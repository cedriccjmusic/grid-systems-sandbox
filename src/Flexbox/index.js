import React from "react";
import {
  TextField,
  Select,
  Autocomplete,
  Button,
} from "@sherwin-williams/mosaic-component-library";
import styled from "styled-components";
import * as mockData from "./mockData.json";

const Box = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  .MuiInput-root,
  .MuiTextField-root {
    margin-bottom: 25px;
  }
  .row {
    position: relative;
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
  }
  .col-6 {
    position: relative;
    width: 50%;
    flex-wrap: nowrap;
    padding: 0 16px;
  }
  .full {
    margin: 0 16px;
  }
  @media (max-width: 640px) {
    .col-6 {
      width: 100%;
    }
  }
`;

export const Flexbox = (props) => {
  return (
    <Box>
      <div className="row">
        <div class="col-6">
          <TextField label="Full Name" width="100%" placeholder="" />
          <TextField label="Email" width="100%" placeholder="" />
        </div>
        <div className="col-6">
          <Select
            width="100%"
            id="projects"
            classes="sample-class"
            options={mockData[1].projects}
            label="Projects"
            placeholder="Select a project"
          />
          <Autocomplete
            label="Supplies"
            placeholder="Select a supply"
            errorText="Error Text"
            options={mockData[0].supplies}
          />
        </div>
      </div>
      <div className="full">
        <TextField
          label="Project Description"
          width="100%"
          multiline
          rows={4}
        />
        <Button text="Submit" style={{ float: "right" }} />
      </div>
    </Box>
  );
};

export default Flexbox;
