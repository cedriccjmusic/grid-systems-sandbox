import React from "react";
import { Container, Row, Col } from "react-grid-system";
import {
  TextField,
  Select,
  Autocomplete,
  Button,
} from "@sherwin-williams/mosaic-component-library";
import styled from "styled-components";
import * as mockData from "./mockData.json";

const Box = styled(Container)`
  padding: 15px;
  .MuiInput-root,
  .MuiTextField-root {
    margin-bottom: 25px;
  }
`;

export const ReactGrid1 = (props) => {
  return (
    <Box>
      <Row>
        <Col sm={6}>
          <Row>
            <Col sm={12}>
              <TextField label="Full Name" width="100%" placeholder="" />
              <TextField label="Email" width="100%" placeholder="" />
            </Col>
          </Row>
        </Col>
        <Col sm={6}>
          <Row>
            <Col sm={12}>
              <Select
                width="100%"
                id="projects"
                classes="sample-class"
                options={mockData[1].projects}
                label="Projects"
                placeholder="Select a project"
              />
              <Autocomplete
                label="Supplies"
                placeholder="Select a supply"
                errorText="Error Text"
                options={mockData[0].supplies}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <TextField
            label="Project Description"
            width="100%"
            multiline
            rows={4}
          />

          <Row justify="end">
            <Col sm={12}>
              <Button text="Submit" style={{ float: "right" }} />
            </Col>
          </Row>
        </Col>
      </Row>
    </Box>
  );
};

export default ReactGrid1;
