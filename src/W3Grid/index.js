import React from "react";

import "./w3pro.css";
import {
  TextField,
  Select,
  Autocomplete,
  Button,
} from "@sherwin-williams/mosaic-component-library";
import styled from "styled-components";
import * as mockData from "./mockData.json";

const Box = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  .MuiInput-root,
  .MuiTextField-root {
    margin-bottom: 25px;
  }
`;

export const ReactGrid1 = (props) => {
  return (
    <Box class=" w3-content">
      <div class="w3-row">
        <div class="w3-half">
          <div class="w3-row-padding">
            <div class="w3-col">
              <TextField label="Full Name" width="100%" placeholder="" />
              <TextField label="Email" width="100%" placeholder="" />
            </div>
          </div>
        </div>
        <div class="w3-half">
          <div class="w3-row-padding">
            <div class="w3-col">
              <Select
                width="100%"
                id="projects"
                classes="sample-class"
                options={mockData[1].projects}
                label="Projects"
                placeholder="Select a project"
              />
              <Autocomplete
                label="Supplies"
                placeholder="Select a supply"
                errorText="Error Text"
                options={mockData[0].supplies}
              />
            </div>
          </div>
        </div>
      </div>
      <div class="w3-row-padding">
        <div class="w3-col">
          <TextField
            label="Project Description"
            width="100%"
            multiline
            rows={4}
          />
          <Button text="Submit" style={{ float: "right" }} />
        </div>
      </div>
    </Box>
  );
};

export default ReactGrid1;
